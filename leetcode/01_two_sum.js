/**
 * time complexity : O(n^2)
 */
var twoSum = function(nums, target) {
  for (var i = 0; i < nums.length; i++) {
    for (var j = i + 1; j < nums.length; j++) {
      if (nums[i] + nums[j] == target) {
        return [i, j];
      }
    }
  }
};

/**
 * time complexity : O(n)
 * space complexity : O(n)
 */
var twoSum2 = function(nums, target) {
  var map = {};
  for (var i = 0; i < nums.length; i++) {
    map[nums[i]] = i;
  }
  for (var i = 0; i < nums.length; i++) {
    var key = target - nums[i];
    // map[key] != i 表示不能重複存取, ex: target = 8, if [2, 4] => 不可有 4 + 4 
    if (map[key] && map[key] != i) {
      return [i, map[key]];
    }
  }
};

/**
 * time complexity : O(n)
 * space complexity : O(n)
 */
var twoSum3 = function(nums, target) {
  var map = {};
  for (var i = 0; i < nums.length; i++) {
    var key = target - nums[i];
    if (map[key] in nums) {
      return [map[key], i];
    }
    map[nums[i]] = i;
  }
};

console.log(twoSum3([3,11,7,2], 9));