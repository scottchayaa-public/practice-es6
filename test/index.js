// 非同步
function addSum(numbers, callback) {
  setTimeout(() => {
    let sum = 0;
    numbers.forEach(number => sum = sum + number);
    callback(sum);
  }, 0);
}
const numbers = [1, 2];
addSum(numbers, sum => {
  console.log(sum);
})
console.log('done');