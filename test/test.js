var Library = {
    name: "Timmy",
    greet: (name) => {
        str = "Hello from the " + name;
        console.log(str);
        //return str;
    },
    greet1: (name, callback) => {
        callback("Hello from the " + name);
    },
    greet2: (name) => {
        return "Hello from the " + name;
    }
};

module.exports.Library = Library;