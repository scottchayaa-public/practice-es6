const lib1 = require('./lib1.js');
const lib2 = require('./lib2.js');
const lib3 = require('./lib3.js');
const lib4 = require('./lib4.js');

const func = () => {
    return 'attr2';
};

var lib = {};
var lib = {
    attr1: 'attr1',
    attr2_1: func,
    attr2_2: func(),
    attr3: () => {
        return 'attr3 (closure)';
    }
};

console.log(lib.attr1);
console.log(lib.attr2_1());
console.log(lib.attr2_2);
console.log(lib.attr3());

console.log('# Case1 ----');

console.log(lib1.attr1);
console.log(lib1.attr2_1());
console.log(lib1.attr2_2);
console.log(lib1.attr3());

console.log('# Case2 ----');

console.log(lib1.attr1);
console.log(lib1.attr2_1());
console.log(lib1.attr2_2);
console.log(lib1.attr3());

console.log('# Case3 ----');

console.log(lib3);
console.log(lib3.attr1);
console.log(lib3.attr2_1());
console.log(lib3.attr2_2);
console.log(lib3.attr3());

console.log('# Case4 ----');

console.log(lib4);
console.log(lib4.attr1);
console.log(lib4.attr2_1());
console.log(lib4.attr2_2);
console.log(lib4.attr3());

