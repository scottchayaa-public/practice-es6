const func = () => {
    return 'attr2';
}

// Case3 : This work
exports.attr1 = 'attr1';
exports.attr2_1 = func;
exports.attr2_2 = func();
exports.attr3 = () => {
    return 'attr3 (closure)';
};
