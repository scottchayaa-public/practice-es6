const func = () => {
    return 'attr2';
}

// Case1 : This work
module.exports.attr1 = 'attr1';
module.exports.attr2_1 = func;
module.exports.attr2_2 = func();
module.exports.attr3 = () => {
    return 'attr3 (closure)';
};
