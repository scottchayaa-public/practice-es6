

const func = () => {
    return 'attr2';
}

// Case4 : Not work!
exports = {
    attr1: 'attr1',
    attr2_1: func,
    attr2_2: func(),
    attr3: () => {
        return 'attr3 (closure)';
    }
};

