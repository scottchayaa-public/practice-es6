
const format_command_space = str => {
    if (str.indexOf("  ") >= 0) {
        return format_command_space(str.replace("  ", " "));
    }
    return str;
}

let str = ' Hello    world    !   ';

console.log(format_command_space(str.trim()));