/**
 * https://developer.mozilla.org/zh-TW/docs/Web/JavaScript/Reference/Global_Objects/Promise
 */
var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;

var promise1 = new Promise(function(resolve, reject) {
  setTimeout(function() {
    resolve('foo');
  }, 1000);
})
.then( result => {
  console.log(result);
})
.catch( error => {
  console.log(error);
});

function myAsyncFunction(url) {
  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    xhr.open("GET", url);
    xhr.onload = () => resolve(xhr.responseText);
    xhr.onerror = () => reject(xhr.statusText);
    xhr.send();
  });
};

console.log(promise1);
myAsyncFunction('https://blog.scottchayaa.com')
.then( response => {
  console.log(response);
});