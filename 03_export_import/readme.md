# Node 使用 es6 export, import的研究

# Install es-checker
```
npm install es-checker -g
```

檢查node目前支援程度

```
> es-checker

...
...

Module  ------> 可以發現目前node v10對module export import還沒支援
  × Module export command
  × Module import command

=========================================
Passes 39 feature Detections
Your runtime supports 92% of ECMAScript 6
=========================================
```

由於我現在是使用VSCode開發nodejs
在使用 `require`, `export` 其他檔案時並沒有問題  
但如果使用 `import`, `module`, `export default` 就會發生 `Syntas error`的錯誤
原因就是 node 目前對 es6 還沒支援完整   

所以在網路上找資料時常常會疑惑  
有的人使用 require, 有的人使用 import  
啊為什麼我使用import都一直出錯  
後來找了很多資料後才知道真相  

OK, 那如果還是要使用import功能的話該怎麼辦 ?
這時候就需要使用 `babel` 相關套件來進行 noew ES6 -> ES5 的轉譯


# Install babel

```
npm install babel-cli babel-present-env --save-dev
```

設定 `.babelrc`

```
{
    "presets": [
        "env"
    ]
}
```


# Run node with babel

`package.json` scripts 加上
```
...

"scripts": {
    "babel": "./node_modules/.bin/babel-node ./src/index.js"
}

...
```

針對 `./src/index.js` 進行編譯執行  
babel會根據index.js裡面有import到的js進行編譯  
然後在執行  

> 編譯就是 ES6 -> ES5 , 將他編譯成任何版本都能執行的狀態

```
npm run babel
```




# Reference
 - [Node.js - use of module.exports as a constructor](https://stackoverflow.com/questions/20534702/node-js-use-of-module-exports-as-a-constructor#answer-20534942)
 - [[Node.js] 使用 Babel 做 ES6 開發](https://medium.com/10coding/node-js-%E4%BD%BF%E7%94%A8-babel-%E5%81%9A-es6-%E9%96%8B%E7%99%BC-44b5b9e5f508)
 - [Getting started with Node.js modules: require, exports, imports and beyond](https://adrianmejia.com/blog/2016/08/12/getting-started-with-node-js-modules-require-exports-imports-npm-and-beyond/)
 - [How to use module.exports in Node.js](https://stackabuse.com/how-to-use-module-exports-in-node-js/)
 - [給開發者的網頁技術文件 JavaScript JavaScript 參考文件 陳述式與宣告 export](https://developer.mozilla.org/zh-TW/docs/Web/JavaScript/Reference/Statements/export)
 - [Node.js v11.10.0 Documentation](https://nodejs.org/api/esm.html)