const multiplyByTwo = (num) =>{
    return num * 2;
}

module.exports = {
    multiplyByTwo
};

// export default {
//     multiplyByTwo
// };