class Square {
    constructor(width) {
        this.width = width;
    }

    area() {
        return Math.pow(this.width, 2);
    }
}

module.exports = Square;
//export default Square;

//不能用
// module.exports = { 
//     Square
// };