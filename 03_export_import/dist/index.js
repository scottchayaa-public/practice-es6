"use strict";

var _multiplyByTwo = require("./multiplyByTwo");

var _multiplyByTwo2 = _interopRequireDefault(_multiplyByTwo);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

console.log((0, _multiplyByTwo2.default)(2));

// const multiplyByTwo = require("./multiplyByTwo");
// console.log(multiplyByTwo(2));