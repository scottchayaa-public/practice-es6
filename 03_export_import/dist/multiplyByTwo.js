"use strict";

var multiplyByTwo = function multiplyByTwo(numberToMultipy) {
    return numberToMultipy * 2;
};
module.exports = multiplyByTwo;