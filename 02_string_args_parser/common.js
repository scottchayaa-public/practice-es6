/**
 * Make n space become to 1 on string.
 */
module.exports.format_command_space = str => {
    if (str.indexOf("  ") >= 0) {
        return this.format_command_space(str.replace("  ", " "));
    }
    return str;
}

/**
 * Random 0 ~ num
 */
module.exports.getRandom = num => {
    return Math.floor(Math.random() * (num + 1) );
};