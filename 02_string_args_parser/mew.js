const minimist = require('minimist');
const common = require('./common');

module.exports.run = (str, callback) => {
    let ary_str = common.format_command_space(str.trim()).split(' ');

    let result = "";
    if (ary_str[0] == 'mew') {
        switch (ary_str[1]) {
            case 'hello': 
                result = this.hello();
                break;
            case 'news': 
                result = this.news(ary_str.slice(2));
                break;
            case 'source': 
                result = this.source(ary_str.slice(2));
                break;
            default:
                result = this.help();
                break;
        }
    }

    callback(result);
}

module.exports.hello = () => {
    var hellos = [
    '幹嘛 ??? ??',
    '快給我飯飯?? !',
    '(眼神死)'
    ];
    
    return hellos[common.getRandom(2)];
}

module.exports.news = (ary) => {
    let args = minimist(ary);
    if (args._.length) {
        console.log('search ' + args._.join(' '));
    } else {
        console.log('no search');
    }
}

module.exports.source = (ary) => {
    return 'source';
}

module.exports.help = () => {
    return 'help';
}

